
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;


/**
 * @author Neza
 * JuliaFrame - graficni vmesnik.
 */


@SuppressWarnings("serial")
public class JuliaFrame extends JFrame implements ActionListener, MouseListener {
	private JuliaPanel slika;
	private JButton gumbNarisi, gumbShrani, gumbPriblizaj, gumbOddalji, gumbPonastavi, gumbPredlog;
	private JTextField cRealnaKomponenta, cImaginarnaKomponenta, iteracije, zacetnaTockaRe, zacetnaTockaIm;
	private static JComboBox<String> barveCb, funkcijeCb;
	public String barvnaPaleta = "Opcija 1";
	public String fraktalizatorskaFunkcija = "z^2 + c";
	
	public Float realni, imaginarni;
	public Complex c = new Complex(0.0, 0.0);
	public Complex zacetnaTocka = new Complex(0.0, 0.0);
	public Integer it = 100;
	public double koeficient = 1.0;
	private GridBagConstraints d;
	
	
	public JuliaFrame() {
		super();
		// Nastavimo naslov okna
		setTitle("JuliaSet");
		slika = new JuliaPanel(512, 512);
		slika.addMouseListener(this);
		// Nastavimo layout manager
		setLayout(new GridBagLayout());
		// Dodamo sliko
		d = new GridBagConstraints();
		d.fill = GridBagConstraints.BOTH;
		d.gridy = 0;
		d.gridx = 0;
		d.weightx = 1.0;
		d.weighty = 1.0;
		d.insets = new Insets(0, 0, 5, 0); // Parametri: (gor,levo,dol,desno)
		d.gridwidth = GridBagConstraints.REMAINDER;
		d.anchor = GridBagConstraints.CENTER;
		add(slika, d);
		
		// Ze na zacetku narisemo neko sliko
		slika.narisi(new Complex(0.0, 0.0), zacetnaTocka, it, (float) 1.0, barvnaPaleta, fraktalizatorskaFunkcija);
		
		// Uporabnik ima par opcij, kjer lahko spreminja funkcijo, ki generira fraktal
		d = new GridBagConstraints();
		d.gridy = 1;
		d.gridx = 0;
		d.insets = new Insets(0, 5, 2, 0);
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Fraktalizatorska funkcija: "), d);
		
		String[] fraktalizatorskeFunkcije = 
			{"z^2 + c", "z^3 + c", "z^4 + c", "z^5 + c", "exp(z^3) + c", "z*exp(z) + c", "(z^2)*exp(z) + c"};
			
		d = new GridBagConstraints();
		d.gridy = 1;
		d.gridx = 1;
		d.insets = new Insets(0, 0, 2, 5);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.gridwidth = GridBagConstraints.REMAINDER;
		funkcijeCb = new JComboBox<String>(fraktalizatorskeFunkcije);
		funkcijeCb.setVisible(true);
		add(funkcijeCb, d);
		
		// Uporabnik poljubno nastavi kompleksno stevilo c
		d = new GridBagConstraints();
		d.gridy = 2;
		d.gridx = 0;
		d.insets = new Insets(0, 5, 0, 0);
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Vnesi kompleksno število c: "), d);
		
		// Dodamo 2 polja za vpis kompleksnega stevila
		// Vnosno polje za realno komponento
		d = new GridBagConstraints();
		d.gridy = 2;
		d.gridx = 1;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.weightx = 1.0;
		cRealnaKomponenta = new JTextField();
		cRealnaKomponenta.setText("0");
		add(cRealnaKomponenta, d);
		// Vmes dodamo se +
		d = new GridBagConstraints();
		d.gridy = 2;
		d.gridx = 2;
		add(new JLabel(" + "), d);
		// Se drugo vnosno polje za imaginarno komponento
		d = new GridBagConstraints();
		d.gridy = 2;
		d.gridx = 3;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.weightx = 1.0;
		cImaginarnaKomponenta = new JTextField();
		cImaginarnaKomponenta.setText("0");
		add(cImaginarnaKomponenta, d);
		// Na koncu dodamo se i
		d = new GridBagConstraints();
		d.gridy = 2;
		d.gridx = 4;
		add(new JLabel("i  "), d);
		
		// Dodamo gumb, ki bo uporabniku vnesel eno konstanto, ki narise kul slikico
		d = new GridBagConstraints();
		d.gridy = 3;
		d.gridx = 0;
		d.insets = new Insets(2, 5, 2, 5);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.gridwidth = GridBagConstraints.REMAINDER;
		d.anchor = GridBagConstraints.CENTER;
		gumbPredlog = new JButton("Nimaš ideje za konstanto? Klikni! :)");
		gumbPredlog.addActionListener(this); 
		add(gumbPredlog, d);
		
		// Uporabnik lahko poljubno nastavi stevilo iteracij
		d = new GridBagConstraints();
		d.gridy = 4;
		d.gridx = 0;
		d.insets = new Insets(0, 5, 0, 0);
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Vnesi število iteracij: "), d);
		
		// Vnosno polje za stevilo iteracij
		d = new GridBagConstraints();
		d.gridy = 4;
		d.gridx = 1;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.weightx = 1.0;
		iteracije = new JTextField();
		iteracije.setText("100");
		add(iteracije, d);
		// Uporabnika opozorimo na privzeto vrednost st. iteracij
		d = new GridBagConstraints();
		d.gridy = 5;
		d.gridx = 0;
		d.anchor = GridBagConstraints.EAST;
		JLabel label1 = new JLabel("Privzeta vrednost: ");
		label1.setFont(new Font("Tahoma", Font.ITALIC, 10));
		add(label1, d);
		d = new GridBagConstraints();
		d.gridy = 5;
		d.gridx = 1;
		d.anchor = GridBagConstraints.WEST;
		JLabel label2 = new JLabel("100 ");
		label2.setFont(new Font("Tahoma", Font.ITALIC, 10));
		add(label2, d);
		
		// Postavimo koordinatni sistem
		// Uporabnik nastavi zacetno tocko
		d = new GridBagConstraints();
		d.gridy = 6;
		d.gridx = 0;
		d.insets = new Insets(0, 5, 0, 0);
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Premikajoča točka: "), d);
		
		// Dodamo 2 polja za vpis kompleksnega �tevila
		// Vnosno polje za realno komponento
		d = new GridBagConstraints();
		d.gridy = 6;
		d.gridx = 1;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.weightx = 1.0;
		zacetnaTockaRe = new JTextField();
		zacetnaTockaRe.setText("0");
		add(zacetnaTockaRe, d);
		// Vmes dodamo se +
		d = new GridBagConstraints();
		d.gridy = 6;
		d.gridx = 2;
		add(new JLabel(" + "), d);
		// Se drugo vnosno polje za imaginarno komponento
		d = new GridBagConstraints();
		d.gridy = 6;
		d.gridx = 3;
		d.fill = GridBagConstraints.HORIZONTAL;
		d.weightx = 1.0;
		zacetnaTockaIm = new JTextField();
		zacetnaTockaIm.setText("0");
		add(zacetnaTockaIm, d);
		// Na koncu dodamo se i
		d = new GridBagConstraints();
		d.gridy = 6;
		d.gridx = 4;
		add(new JLabel("i  "), d);
		// Uporabnika opozorimo na privzeto vrednost zacetne tocke
		d = new GridBagConstraints();
		d.gridy = 7;
		d.gridx = 0;
		d.anchor = GridBagConstraints.EAST;
		JLabel label3 = new JLabel("Privzeta vrednost: ");
		label3.setFont(new Font("Tahoma", Font.ITALIC, 10));
		add(label3, d);
		d = new GridBagConstraints();
		d.gridy = 7;
		d.gridx = 1;
		d.gridwidth = 3;
		d.anchor = GridBagConstraints.WEST;
		JLabel label4 = new JLabel("0+0i; premikajoča točka služi premikanju levo in desno po sliki fraktala. ");
		label4.setFont(new Font("Tahoma", Font.ITALIC, 10));
		add(label4, d);
		
		// Dodamo gumb za izris fraktala
		d = new GridBagConstraints();
		d.gridy = 8;
		d.gridx = 0;
		d.insets = new Insets(2, 5, 2, 5);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.gridwidth = GridBagConstraints.REMAINDER;
		d.anchor = GridBagConstraints.CENTER;
		gumbNarisi = new JButton("NARIŠI");
        InputMap im = gumbNarisi.getInputMap();
        im.put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        im.put(KeyStroke.getKeyStroke("released ENTER"), "released");
		gumbNarisi.addActionListener(this);
		add(gumbNarisi, d);	
		getRootPane().setDefaultButton(gumbNarisi);
		
		// Uporabnik ima par opcij, kjer lahko spreminja barvno paleto
		d = new GridBagConstraints();
		d.gridy = 9;
		d.gridx = 0;
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Barvna paleta: "), d);
		
		String[] barvnePalete = {"Opcija 1", "Opcija 2", "Opcija 3", "Opcija 4"};
			
		d = new GridBagConstraints();
		d.gridy = 9;
		d.gridx = 1;
		d.fill = GridBagConstraints.HORIZONTAL;
		barveCb = new JComboBox<String>(barvnePalete);
		barveCb.setVisible(true);
		add(barveCb, d);
		
		// Uporabnik lahko sliko pribliza ali oddalji
		d = new GridBagConstraints();
		d.gridy = 10;
		d.gridx = 0;
		d.insets = new Insets(2, 0, 0, 0);
		d.anchor = GridBagConstraints.EAST;
		add(new JLabel("Približaj ali oddalji: "), d);
		
		// Dodamo gumb za povecanje slike
		d = new GridBagConstraints();
		d.gridy = 10;
		d.gridx = 1;
		d.insets = new Insets(2, 0, 0, 0);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.anchor = GridBagConstraints.CENTER;
		gumbPriblizaj = new JButton("Približaj");
		gumbPriblizaj.addActionListener(this); 
		add(gumbPriblizaj, d);
		
		// Dodamo gumb za pomanjsanje slike
		d = new GridBagConstraints();
		d.gridy = 10;
		d.gridx = 3;
		d.insets = new Insets(2, 0, 0, 0);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.anchor = GridBagConstraints.CENTER;
		gumbOddalji = new JButton("Oddalji");
		gumbOddalji.addActionListener(this); 
		add(gumbOddalji, d);
		
		// Uporabnika opozorimo na možnosti povecevanja in oddaljevanja
		d = new GridBagConstraints();
		d.gridy = 11;
		d.gridx = 0;
		d.gridwidth = 3;
		d.anchor = GridBagConstraints.EAST;
		JLabel label5 = new JLabel("Sliko lahko približaš tudi s klikom nanjo. ");
		label5.setFont(new Font("Tahoma", Font.ITALIC, 10));
		add(label5, d);
		
		// Dodamo gumb za ponastavitev parametrov
		d = new GridBagConstraints();
		d.gridy = 12;
		d.gridx = 0;
		d.insets = new Insets(2, 5, 2, 5);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.gridwidth = GridBagConstraints.REMAINDER;
		d.anchor = GridBagConstraints.CENTER;
		gumbPonastavi = new JButton("Ponastavi parametre!");
		gumbPonastavi.addActionListener(this); 
		add(gumbPonastavi, d);
		
		// Dodamo gumb za shranjevanje slike
		d = new GridBagConstraints();
		d.gridy = 13;
		d.gridx = 0;
		d.insets = new Insets(0, 5, 5, 5);
		d.fill = GridBagConstraints.HORIZONTAL;
		d.gridwidth = GridBagConstraints.REMAINDER;
		d.anchor = GridBagConstraints.CENTER;
		gumbShrani = new JButton("Shrani sliko");
		gumbShrani.addActionListener(this); 
		add(gumbShrani, d);
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		barvnaPaleta = barveCb.getSelectedItem().toString();
		fraktalizatorskaFunkcija = funkcijeCb.getSelectedItem().toString();
		
        if (e.getSource() == gumbNarisi) {
        	try {
        		realni = new Float(cRealnaKomponenta.getText());
        		imaginarni = new Float(cImaginarnaKomponenta.getText());
        		c = new Complex(realni, imaginarni);
        		
        		if (iteracije.getText().equals("")) {
        			it = 100;
        		}
        		else {
            		it = new Integer(iteracije.getText());
        		}
        		
        		if (zacetnaTockaRe.getText().equals("") && zacetnaTockaIm.getText().equals("")) {
        			zacetnaTocka = new Complex(0,0); 
        		}
        		else {
            		realni = new Float (zacetnaTockaRe.getText());
            		imaginarni = new Float(zacetnaTockaIm.getText());
            		zacetnaTocka = new Complex (realni, imaginarni);
        		}
        		
        		System.out.println("Klik na gumb nariši, slika se riše ...");
        		// Slika naj se narise
        		slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
        	}
        	catch (NumberFormatException exc) {
        		// Ce se uporabnik zatipka pri iteracijah ali zacetni tocki,
        		// bomo sliko vseeno narisali s privzetimi vrednostmi
        		it = 100;
        		zacetnaTocka = new Complex(0,0);
        		try {
        			slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
        		}
        		catch (NumberFormatException exc1) {
        			// Ce uporabnik ne vnese stevilke za c ali se zatipka, mu to javimo
        			JOptionPane.showMessageDialog(this, 
        					"Preveri, če si pravilno vnesel parametre.",
        					"Izris fraktala ni mogoč", JOptionPane.ERROR_MESSAGE);
        		}
        	}
        }
        
        if (e.getSource() == gumbPredlog) {
            String[] fraktalizatorskeFunkcije = 
        		{"z^2 + c", "z^3 + c", "z^4 + c", "z^5 + c", "exp(z^3) + c", "z*exp(z) + c", "(z^2)*exp(z) + c"};
            if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[0]) {
            	cRealnaKomponenta.setText("-0.621");
            	cImaginarnaKomponenta.setText("0.421");
    	    }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[1]) {
            	cRealnaKomponenta.setText("0.400");
            	cImaginarnaKomponenta.setText("0.000");
            }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[2]) {
            	cRealnaKomponenta.setText("0.484");
            	cImaginarnaKomponenta.setText("0.000");
            }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[3]) {
            	cRealnaKomponenta.setText("0.544");
            	cImaginarnaKomponenta.setText("0.000");
            }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[4]) {
            	cRealnaKomponenta.setText("-0.590");
            	cImaginarnaKomponenta.setText("0.000");
            }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[5]) {
            	cRealnaKomponenta.setText("0.040");
            	cImaginarnaKomponenta.setText("0.000");
            }
            else if (fraktalizatorskaFunkcija == fraktalizatorskeFunkcije[6]) {
            	cRealnaKomponenta.setText("0.210");
            	cImaginarnaKomponenta.setText("0.000");
            }
        }
         
        if (e.getSource() == gumbPriblizaj) {
            // Slika naj se poveca
            koeficient = koeficient/Math.sqrt(2);
            System.out.println(koeficient);
            slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
        }
        
        if (e.getSource() == gumbOddalji) {
            // Slika naj se pomanjsa
            koeficient = koeficient*Math.sqrt(2);
            System.out.println(koeficient);
            slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
        }
       
        if (e.getSource() == gumbPonastavi) {
    	    // Vsi parametri naj se ponastavijo, narise naj se osnovna zacetna slika
    	    c = new Complex(0.0, 0.0);
    	    zacetnaTocka = new Complex(0.0, 0.0);
    	    it = 100;
    	    koeficient = 1.0;
    	    barvnaPaleta = "Opcija 1";
   		    fraktalizatorskaFunkcija = "z^2 + c";
    	    cRealnaKomponenta.setText("0");
    	    cImaginarnaKomponenta.setText("0");
    	    zacetnaTockaRe.setText("0");
    	    zacetnaTockaIm.setText("0");
    	    iteracije.setText(it.toString());
    	    barveCb.setSelectedItem("Opcija 1");
    	    funkcijeCb.setSelectedItem("z^2 + c");
    	    slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
        }
        
        if (e.getSource() == gumbShrani) {
    	    // Slika naj se shrani
    	    slika.shraniSliko();
        } 
    }


	public void mouseClicked(MouseEvent e) {
		// Sliko lahko med drugim povecujemo tudi s klikom nanjo
		int x = e.getX();
		int y = e.getY();
		System.out.println("kliknili smo na: x = " + x +", y = " + y);
		koeficient = koeficient/Math.sqrt(2);
		realni = new Float ((float) x*4/512 - 2); 
		imaginarni = new Float((float) (y+5)*4/512 - 2); // +5, ker slika na intervalu [5, 517]
		zacetnaTocka = new Complex(realni, imaginarni);
		zacetnaTockaRe.setText(realni.toString());
		zacetnaTockaIm.setText(imaginarni.toString());
		System.out.println(zacetnaTocka);
        slika.narisi(c, zacetnaTocka, it, koeficient, barvnaPaleta, fraktalizatorskaFunkcija);
	}


	public void mouseEntered(MouseEvent arg0) {
		
	}


	public void mouseExited(MouseEvent arg0) {
		
	}


	public void mousePressed(MouseEvent arg0) {
		
	}


	public void mouseReleased(MouseEvent arg0) {
		
	}
	
}