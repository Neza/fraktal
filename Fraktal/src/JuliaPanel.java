
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 * @author Neza
 * JuliaPanel - risarski del.
 */


@SuppressWarnings("serial")
public class JuliaPanel extends JPanel {
	private BufferedImage slika;
	private boolean ustavi;
	private Thread slikarThread;
	
	
	static String[] fraktalizatorskeFunkcije = 
		{"z^2 + c", "z^3 + c", "z^4 + c", "z^5 + c", "exp(z^3) + c", "z*exp(z) + c", "(z^2)*exp(z) + c"};
	
	
	/**
	 * Vrne stevilo iteracij, da preverimo,
	 * ce je z se v Juliajevi mnozici.
	 * @param c
	 * @param z
	 * @param maxiter - stevilo iteracij
	 * @param ff - fraktalizatorska funkcija (tista, ki bo generirala fraktal)
	 * @return
	 */	
	static int julia(Complex c, Complex z, int maxiter, String ff) {
		for (int t = 1; t < maxiter; t++) {
			if (z.abs() > 2.0) return t;
			
			if (ff == fraktalizatorskeFunkcije[0]) {
				z = z.times(z).plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[1]) {
				z = z.times(z).times(z).plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[2]) {
				z = z.times(z).times(z).times(z).plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[3]) {
				z = z.times(z).times(z).times(z).times(z).plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[4]) {
				z = (z.times(z).times(z)).exp().plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[5]) {
				z = z.times(z.exp()).plus(c);
			}
			if (ff == fraktalizatorskeFunkcije[6]) {
				z = z.exp().times(z).times(z).plus(c);
			}
		}
		return 0;
	}
	
	
	public JuliaPanel(int width, int height) {
		super();
		this.slika = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	}
	
	
	/**
	 * Ta metoda izracuna sliko in *blokira* celoten program med racunanjem.
	 * @param c konstanta
	 * @param zacetnaTocka - sredisce koordinatnega sistema
	 * @param it - stevilo iteracij
	 * @param koeficient - koeficient, s katerim sliko priblizamo ali oddaljimo
	 * @param bp - barvna paleta, s katero se fraktal izrise
	 */
	public void izracunajSliko(Complex c, Complex zacetnaTocka, Integer it, double koeficient, String bp, String ff) {
		try{
			for (int x=0; x < slika.getWidth(); x++) {
				repaint();
				if (ustavi) {
					return;
				}
				for (int y=0; y < slika.getHeight(); y++) {
					double m = (slika.getWidth() + slika.getHeight())/2;
					Complex z = new Complex(koeficient*(4.0*x/m+zacetnaTocka.re()-2.0), koeficient*(4.0*y/m+zacetnaTocka.im()-2.0)); // Mnozica se nahaja v krogu s polmerom 2
					int iters = julia(c, z, it, ff);	
					if (iters == 0) {
						slika.setRGB(x, y, 0); // Notranjost pobarvaj crno
					}
					else {
						if (bp == "Opcija 1") {
							int red = (100*iters - 450);
							int green = iters;
							int blue = 10*iters;
							int rgb = (red << 16) | (green << 8) | blue ; 
							slika.setRGB(x, y, rgb);
						}
						if (bp == "Opcija 2") {
							int red1 = (int) (((double)iters/it)*255);
							int green1 = (int) (((double)iters/it)*255);
							int blue1 = (int) (((double)iters/it)*255)*3;
							int rgb1 = (red1 << 16) | (green1 << 8) | blue1 ; 
							slika.setRGB(x, y, rgb1);
						}
						if (bp == "Opcija 3") {
							int red2 = (int) (((double)iters/it)*255)*2-450;
							int green2 = (int) (((double)iters/it)*255)*10;
							int blue2 = (int) (((double)iters/it)*255)+100;
							int rgb2 = (red2 << 16) | (green2 << 8) | blue2 ; 
							slika.setRGB(x, y, rgb2);
						}
						if (bp == "Opcija 4") {
							int red3 = (100*iters - 450);
							int green3 = 10*iters;
							int blue3 = iters;
							int rgb3 = (red3 << 16) | (green3 << 8) | blue3 ; 
							slika.setRGB(x, y, rgb3);
						}
					}
				}
			}
		}			
		catch (NullPointerException exc3) {
			// Ce uporabnik kaksen podatek izpusti ali se zatipka, mu javi sporocilo
			JOptionPane.showMessageDialog(this, 
					"Preveri, �e si pravilno vnesel parametre.",
					"Izris fraktala ni mogo�", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	/**
	 * Narise sliko v vzporednem vlaknu, da ne bo program "zmrznil"
	 * med racunanjem slike.
	 */
	public void narisi(final Complex c, final Complex zacetnaTocka, final Integer it, final double koeficient, final String bp, final String ff) {
		if (slikarThread != null) {
			ustavi = true;
			try {
				slikarThread.join(); // pocakam, da se ustavi
			} catch (InterruptedException e) {
				// delamo se, da je vse ok;
			} 
			slikarThread = null;
		}
		Runnable slikar = new Runnable() {
			public void run() {
				izracunajSliko(c, zacetnaTocka, it, koeficient, bp, ff);
				repaint();
			}
		};
		ustavi = false;
		slikarThread = new Thread(slikar);
		slikarThread.start();
		
	}
	
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(slika.getWidth(), slika.getHeight());
	}

	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = slika.getWidth();
		int h = slika.getHeight();
		int x = (this.getWidth() - w)/2;
		int y = (this.getHeight() - h)/2;
		g.drawImage(slika, x, y, w, h, Color.BLACK, null);
	}
	
	
	public void shraniSliko(){
	    try{
	      File f = new File("D:\\SlikaJuliaSet"+ Math.random() +".png");  // Mesto, kamor se slika shrani
	      ImageIO.write(slika, "png", f);
	      System.out.println("Shranjevanje slike je uspelo.");
	    }
	    catch (IOException e) {
	      System.out.println("Napaka: " + e);
	    }
	  }
}