
import javax.swing.JFrame;


/**
 * @author Neza
 * Julia set - glavni program.
 */


public class JuliaSet {

	public static void main(String[] args) {
		JFrame okno = new JuliaFrame();
		okno.pack();
		okno.setVisible(true);
		okno.setResizable(false); // uporabnik ne more spreminjati velikosti okna - ugodneje za racunanje, ko povecujemo sliko s klikom
	}

}