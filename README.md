# README #

### Čemu je ta repozitorij namenjen? ###

* Projektu Java pri predmetu Programiranje 2
* Moj projekt: Fraktal

### Kratek opis projekta ###

Osnovna verzija:

* Program izriše Juliajevo množico za dane parametre.
* Funkciji f(z) = z^2 + c spreminjamo parameter c in v odvisnosti od c izrišemo Juliajevo množico. Spreminjamo lahko tudi število iteracij.

Razširitve:

* Uporabnik lahko poveča/pomanjša sliko s klikom na gumb.
* Izvoz slike v datoteko.
* Uporabnik lahko izbere barvno paleto, s katero je narisan fraktal.

### Vsebina repozitorija: ###

* `JuliaSet`: glavno okno
* `JuliaFrame`: grafični vmesnik, interakcija z uporabnikom
* `JuliaPanel`: risarski del, izris fraktala, računanje slike
* `Complex.java`: definicija kompleksnih števil

### Več informacij ... ###

* neza.dimec@student.fmf.uni-lj.si